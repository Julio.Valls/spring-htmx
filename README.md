# Spring HTMX

## Setup

- Run `./start_potgres.sh` to start the progress container.

- Run the SQL in the files `schema.sql` and `data.sql` to populate the database.

- Start the spring boot application

- Navigate to `localhost:3003/guests`