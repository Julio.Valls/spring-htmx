#!/usr/bin/env bash

set -euo pipefail

mkdir -p "$HOME"/.local/docker/postgresql

docker run --rm --name pg-docker -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=local -d -p 5434:5432 postgres
