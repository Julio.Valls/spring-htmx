package com.julio.lil.landonhotel.service;

import com.julio.lil.landonhotel.data.entity.Guest;
import com.julio.lil.landonhotel.data.entity.Reservation;
import com.julio.lil.landonhotel.data.entity.Room;
import com.julio.lil.landonhotel.data.repository.GuestRepository;
import com.julio.lil.landonhotel.data.repository.ReservationRepository;
import com.julio.lil.landonhotel.data.repository.RoomRepository;
import com.julio.lil.landonhotel.service.model.RoomReservation;
import io.micrometer.common.util.StringUtils;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class RoomReservationService {
	private final GuestRepository guestRepository;
	private final RoomRepository roomRepository;
	private final ReservationRepository reservationRepository;

	public RoomReservationService(GuestRepository guestRepository, RoomRepository roomRepository, ReservationRepository reservationRepository) {
		this.guestRepository = guestRepository;
		this.roomRepository = roomRepository;
		this.reservationRepository = reservationRepository;
	}

	public List<RoomReservation> getRoomReservationsForDate(String reservationDate) {
		Date date = StringUtils.isNotEmpty(reservationDate) ? Date.valueOf(reservationDate) : new Date(System.currentTimeMillis());
		Map<Long, RoomReservation> roomReservationMap = new HashMap<>();
		List<Room> rooms = this.roomRepository.findAll();

		rooms.forEach(room -> {
			RoomReservation roomReservation = new RoomReservation();
			roomReservation.setRoomId(room.getId());
			roomReservation.setRoomName(room.getName());
			roomReservation.setRoomNumber(room.getRoomNumber());
			roomReservationMap.put(roomReservation.getRoomId(), roomReservation);
		});

		List<Reservation> reservations = this.reservationRepository.findAllByReservationDate(date);

		reservations.forEach(reservation -> {
			RoomReservation roomReservation = roomReservationMap.get(reservation.getRoomId());
			roomReservation.setReservationId(reservation.getReservationId());
			roomReservation.setReservationDate(reservation.getReservationDate().toString());
			Optional<Guest> guest = this.guestRepository.findById(reservation.getGuestId());
			roomReservation.setGuestId(guest.get().getGuestId());
			roomReservation.setFirstName(guest.get().getFirstName());
			roomReservation.setLastName(guest.get().getLastName());
		});
		return roomReservationMap.values().stream().toList();
	}
}
