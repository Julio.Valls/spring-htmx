package com.julio.lil.landonhotel.data.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Entity
@Table(name = "rooms")
@Getter
@ToString
public class Room {

	@Id
	@Column(name = "room_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "room_number")
	private String roomNumber;

	@Column(name = "bed_info")
	private String bedInfo;
}
