package com.julio.lil.landonhotel.data.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.ToString;

import java.sql.Date;

@Entity
@Table(name = "reservations")
@Getter
@ToString
public class Reservation {

	@Id
	@Column(name = "reservation_id")
	private long reservationId;

	@Column(name = "room_id")
	private long roomId;

	@Column(name = "guest_id")
	private long guestId;

	@Column(name = "res_date")
	private Date reservationDate;
}
