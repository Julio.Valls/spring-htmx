package com.julio.lil.landonhotel.data.repository;

import com.julio.lil.landonhotel.data.entity.Guest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GuestRepository extends JpaRepository<Guest, Long> {

	Optional<Guest> findByEmailAddress(String emailAddress);

	List<Guest> findAllByOrderByGuestIdDesc();

	List<Guest> findByFirstNameIgnoreCaseOrLastNameIgnoreCase(String firstName, String lastName);
}
