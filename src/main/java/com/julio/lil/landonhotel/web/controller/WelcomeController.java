package com.julio.lil.landonhotel.web.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import static io.micrometer.common.util.StringUtils.isNotBlank;
import static java.lang.StringTemplate.STR;

@Controller
@RequestMapping("/welcome")
public class WelcomeController {

	@GetMapping(produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String getWelcome(@RequestParam(value= "name", required = false) String name) {
		String guestName = isNotBlank(name) ? name : "Guest";
		return STR."<h1>Welcome to the Landon Hotel, \{guestName}!</h1>";
	}
}
