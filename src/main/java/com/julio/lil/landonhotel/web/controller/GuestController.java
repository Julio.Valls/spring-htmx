package com.julio.lil.landonhotel.web.controller;

import com.julio.lil.landonhotel.data.entity.Guest;
import com.julio.lil.landonhotel.data.repository.GuestRepository;
import com.julio.lil.landonhotel.web.exception.BadRequestException;
import io.github.wimdeblauwe.htmx.spring.boot.mvc.HxRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/guests")
public class GuestController {

	private final GuestRepository guestRepository;

	public GuestController(GuestRepository guestRepository) {
		this.guestRepository = guestRepository;
	}

	@GetMapping
	public String getGuests(Model model) {
		Guest guest = new Guest();
		model.addAttribute("guest", guest);
		model.addAttribute("guests", this.guestRepository.findAllByOrderByGuestIdDesc());
		return "guest-list";
	}

	@GetMapping("/{id}")
	@HxRequest
	public String getGuest(@PathVariable long id, Model model) {
		Guest guest = this.guestRepository.findById(id).orElseThrow(() -> new BadRequestException("The Guest does not exist!"));
		model.addAttribute("guest", guest);
		return "fragments/guests :: guest-entry";
	}

	@PostMapping
	@HxRequest
	public String addGuest(Guest guest, Model model) {
		this.guestRepository.save(guest);
		model.addAttribute("guest", guest);
		return "fragments/guests :: guest-entry";
	}

	@GetMapping("/{id}/edit")
	@HxRequest
	public String editGuest(@PathVariable long id, Model model) {
		Guest guest = this.guestRepository.findById(id).orElseThrow(() -> new BadRequestException("The Guest does not exist!"));
		model.addAttribute("guest", guest);
		return "fragments/guests :: guest-entry-edit";
	}

	@PostMapping("/search")
	@HxRequest
	@ResponseStatus(HttpStatus.ACCEPTED)
	public String searchGuests(@RequestParam(name = "search") String term, Model model) {
		if (term == null || term.isBlank()) {
			model.addAttribute("guests", this.guestRepository.findAllByOrderByGuestIdDesc());
			return "fragments/guests :: guest-entry-list";
		}
		model.addAttribute("guests", this.guestRepository.findByFirstNameIgnoreCaseOrLastNameIgnoreCase(term, term));
		return "fragments/guests :: guest-entry-list";
	}

	@PutMapping("/{id}")
	@HxRequest
	@ResponseStatus(HttpStatus.ACCEPTED)
	public String updateGuest(@PathVariable Long id, Guest guest, Model model) {
		Guest existingGuest = guestRepository.findById(id)
			.orElseThrow(() -> new BadRequestException("The Guest does not exist!"));

		existingGuest.setFirstName(guest.getFirstName());
		existingGuest.setLastName(guest.getLastName());
		existingGuest.setPhoneNumber(guest.getPhoneNumber());

		this.guestRepository.save(existingGuest);
		model.addAttribute("guest", existingGuest);
		return "fragments/guests :: guest-entry";
	}

	@DeleteMapping("/{id}")
	@HxRequest
	@ResponseStatus(HttpStatus.ACCEPTED)
	public String deleteGuest(@PathVariable long id) {
		try {
			this.guestRepository.deleteById(id);
		} catch (Exception e) {
			throw new BadRequestException("The Guest could not be deleted!");
		}
		return null;
	}
}
