package com.julio.lil.landonhotel.web.controller;

import com.julio.lil.landonhotel.service.RoomReservationService;
import io.micrometer.common.util.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/room/reservation")
public class RoomReservationController {

	RoomReservationService roomReservationService;

	public RoomReservationController(RoomReservationService roomReservationService) {
		this.roomReservationService = roomReservationService;
	}

	@GetMapping
	public String getReservations(Model model, @RequestParam(value = "date", required = false) String dateParam) {
		Date date = new Date();
		if (StringUtils.isNotBlank(dateParam)) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				date = formatter.parse(dateParam);
			} catch (Exception _) {
			}
		}

		model.addAttribute("date", date);
		model.addAttribute("reservations", this.roomReservationService.getRoomReservationsForDate(dateParam));
		return "room-reservations";
	}
}
